# TrieRust

A simple project for Rust beginner :D

## Input

a list of words `words`, a natural number `numCandidate` 

## Task-1: Autocomplete

Given a prefix of any word `prefix`, output a list of `numCandidate` candidate words from `words` in descending order of frequency. 

## Task-2: Multi-autocomplete

Multiple thread doing autocomplete task.

## Task-3: Single-add, Multi-autocomple

One thread adding a new word to `words` each time, multiple threads doing autocomplete task.
